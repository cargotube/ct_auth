-module(cta_session).

-include("ct_auth.hrl").

-export([new/4, close/1, set_id/2, set_auth_details/4, authenticate/2, add_subscription/2,
         has_subscription/2, remove_subscription/2, add_registration/2, has_registration/2,
         remove_registration/2, add_invocation/2, has_invocation/2, remove_invocation/2,
         get_details/1, get_authz/1, get_id/1, get_realm/1, get_subscriptions/1,
         get_registrations/1, get_invocations/1, is_disclose_caller/1, is_disclose_publisher/1,
         is_authenticated/1, get_authid/1, get_authrole/1, gen_global_id/0]).

new(IP, Port, Cert, Transport) ->
    #cta_session{id = undefined,
                 peer_ip = IP,
                 peer_port = Port,
                 peer_cert = Cert,
                 transport = Transport}.

set_id(Id, Session) ->
    Session#cta_session{id = Id, authenticated = true}.

close(_Session) ->
    ok.

set_auth_details(AuthId, AuthMethod, AuthProvider, Session) ->
    Session#cta_session{authid = AuthId,
                        authmethod = AuthMethod,
                        authprovider = AuthProvider}.

authenticate(AuthRole, Session) ->
    RoleName = cta_role:name(AuthRole),
    Authz = cta_role:authz(AuthRole),
    Session#cta_session{authrole = RoleName, authz = Authz, authenticated = true}.

add_subscription(SubId, #cta_session{subscriptions = Subs} = Session) ->
    NewSubs = [SubId | lists:delete(SubId, Subs)],
    Session#cta_session{subscriptions = NewSubs}.

has_subscription(SubId, #cta_session{subscriptions = Subs}) ->
    lists:member(SubId, Subs).

get_subscriptions(#cta_session{subscriptions = Subs}) ->
    Subs.

remove_subscription(SubId, #cta_session{subscriptions = Subs} = Session) ->
    NewSubs = lists:delete(SubId, Subs),
    Session#cta_session{subscriptions = NewSubs}.

add_registration(RegId, #cta_session{registrations = Regs} = Session) ->
    NewRegs = [RegId | lists:delete(RegId, Regs)],
    Session#cta_session{registrations = NewRegs}.

has_registration(RegId, #cta_session{registrations = Regs}) ->
    lists:member(RegId, Regs).

get_registrations(#cta_session{registrations = Regs}) ->
    Regs.

remove_registration(SubId, #cta_session{subscriptions = Subs} = Session) ->
    NewSubs = lists:delete(SubId, Subs),
    Session#cta_session{subscriptions = NewSubs}.

add_invocation(InvocId, #cta_session{invocations = Invocs} = Session) ->
    NewInvocs = [InvocId | lists:delete(InvocId, Invocs)],
    Session#cta_session{invocations = NewInvocs}.

has_invocation(InvocId, #cta_session{invocations = Invocs}) ->
    lists:member(InvocId, Invocs).

get_invocations(#cta_session{invocations = Invocs}) ->
    Invocs.

remove_invocation(InvocId, #cta_session{invocations = Invocs} = Session) ->
    NewInvocs = lists:delete(InvocId, Invocs),
    Session#cta_session{invocations = NewInvocs}.

is_authenticated(#cta_session{authenticated = IsAuth}) ->
    IsAuth.

get_details(#cta_session{details = Details}) ->
    Details.

get_authz(#cta_session{authz = Authz}) ->
    Authz.

get_id(#cta_session{id = Id}) ->
    Id.

get_realm(#cta_session{realm = Realm}) ->
    Realm.

get_authid(#cta_session{authid = Id}) ->
    Id.

get_authrole(#cta_session{authrole = Role}) ->
    Role.

is_disclose_caller(#cta_session{disclose_caller = DiscloseCaller}) ->
    DiscloseCaller.

is_disclose_publisher(#cta_session{disclose_publisher = DisclosePublisher}) ->
    DisclosePublisher.

gen_global_id() ->
    rand:uniform(9007199254740993) - 1.
