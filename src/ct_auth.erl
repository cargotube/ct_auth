-module(ct_auth).

-export([handle_hello/2, handle_authenticate/2]).

-include("ct_auth.hrl").

-include_lib("ct_msg/include/ct_msg.hrl").

handle_hello({hello, RealmName, Details}, _Session) ->
    Result = cta_realm:lookup_for(RealmName, transportId),
    SessionResult = handle_realm(Result, Details, peer, transport),
    return_welcome_challenge_or_abort(SessionResult).

handle_authenticate(_Authenticate, _Session) ->
    {abort, canceled}.

handle_realm({ok, Realm}, Details, Peer, Transp) ->
    IsClosing = cta_realm:is_closing(Realm),
    AuthMethod = get_auth_method(Realm, Details),
    maybe_create_session(IsClosing, AuthMethod, Realm, Details, Peer, Transp);
handle_realm(_Result, _Details, _Peer, _Transport) ->
    {error, no_such_realm}.

maybe_create_session(true, _Methods, _Realm, _Details, _Peer, _Transport) ->
    {error, realm_closing};
maybe_create_session(false, none, _Realm, _Details, _Peer, _Transport) ->
    {error, no_such_auth_method};
maybe_create_session(false, AuthMethod, Realm, Details, Peer, Transport) ->
    RealmName = cta_realm:get_name(Realm),
    {ok, Session} = cta_session:new(RealmName, Details, Peer, Transport),
    {ok, Session, AuthMethod, Realm}.

get_auth_method(Realm, Details) ->
    AuthId = maps:get(<<"authid">>, Details, undefined),
    AuthMethods = get_client_authmethods(Details, AuthId),
    RealmMethods = cta_realm:get_auth_methods(Realm),

    ToAtom =
        fun (Method, List) ->
                try
                  [binary_to_existing_atom(Method, utf8) | List]
                catch
                  _:_ ->
                      List
                end
        end,
    ClientSupported = lists:foldl(ToAtom, [], AuthMethods),

    FindBestMethod =
        fun (Method, Current) ->
                case lists:member(Method, ClientSupported) of
                  true ->
                      Method;
                  false ->
                      Current
                end
        end,
    lists:foldr(FindBestMethod, none, RealmMethods).

get_client_authmethods(Details, undefined) ->
    maps:get(<<"authmethods">>, Details, [<<"anonymous">>]);
get_client_authmethods(Details, _) ->
    maps:get(<<"authmethods">>, Details, []).

-spec return_welcome_challenge_or_abort(SessionResult) -> Result when SessionResult ::
                                                                          {ok,
                                                                           Session,
                                                                           AuthMethod,
                                                                           Realm} |
                                                                          {error, Reason},
                                                                      Session :: cta_session(),
                                                                      AuthMethod :: atom(),
                                                                      Realm :: binary(),
                                                                      Reason :: atom(),
                                                                      Result :: {ok, Session} |
                                                                                {abort, canceled} |
                                                                                {error, Reason}.
return_welcome_challenge_or_abort({ok, Session, AuthMethod, Realm}) ->
    cta_auth_method:welcome_challenge_or_abort(Session, AuthMethod, Realm);
return_welcome_challenge_or_abort({error, realm_closing}) ->
    return_abort(close_realm);
return_welcome_challenge_or_abort({error, no_such_realm}) ->
    return_abort(no_such_realm);
return_welcome_challenge_or_abort({error, no_such_auth_method}) ->
    return_abort(invalid_argument).

% -spec abort_session(Session) -> {abort, canceled}
%  when
%    Session :: cta_session().
% abort_session(Session) ->
%    ok = cta_session:close(Session),
%    return_abort(canceled).

-spec return_abort(Reason) -> {abort, Reason} when Reason :: atom() | binary().
return_abort(Reason) ->
    {abort, Reason}.
