-module(cta_auth_tls).

-export([authenticate/3]).

-include("ct_auth.hrl").

-spec authenticate(Session, Realm, Config) -> Result when Session :: cta_session(),
                                                          Realm :: cta_realm(),
                                                          Config :: map(),
                                                          Result :: {ok, Session} |
                                                                    {error, saving} |
                                                                    {abort, canceled}.
authenticate(Session, Realm, _Config) ->
    {ok, NewSession} = cta_session:set_auth_details(anonymous, anonymous, anonymous, Session),
    RoleResult = cta_realm:get_role(anonymous, Realm),
    maybe_authenticate_session(RoleResult, NewSession).

-spec maybe_authenticate_session(MaybeRole, Session) -> Result when MaybeRole :: {ok,
                                                                                  Role} |
                                                                                 any(),
                                                                    Role :: binary(),
                                                                    Session :: cta_session(),
                                                                    Result :: {ok, Session} |
                                                                              {error, saving} |
                                                                              {abort, cancel}.
maybe_authenticate_session({ok, Role}, Session) ->
    cta_session:authenticate(Role, Session);
maybe_authenticate_session(_, _Session) ->
    {abort, cancel}.
