-module(ct_authz).

-export([is_message_allowed/2]).

-include("ct_auth.hrl").

is_message_allowed(Message, Session) ->
    Type = ct_msg:get_type(Message),
    check_or_allow(Type, Message, Session).

check_or_allow(yield, _, _) ->
    true;
check_or_allow(error, _, _) ->
    true;
check_or_allow(unsubscribe, _, _) ->
    true;
check_or_allow(unregister, _, _) ->
    true;
check_or_allow(Type, Message, Session) ->
    Authz = cta_session:get_authz(Session),
    Uri = ct_msg:get_uri(Message),
    is_message_allowed(Authz, Type, Uri).

is_message_allowed({static, Perm}, Type, Uri) ->
    is_static_allowed(Type, Uri, Perm);
is_message_allowed({dynamic, _Perm}, _Type, _Uri) ->
    false.

is_static_allowed(Type, {ok, Uri}, Perm) ->
    allowed == cta_permission:get_perm_for(Type, Uri, Perm);
is_static_allowed(_Type, _Message, _Perm) ->
    false.
