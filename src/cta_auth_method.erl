-module(cta_auth_method).

-export([create_methods/2, welcome_challenge_or_abort/3]).

-include("ct_auth.hrl").

-callback create(Config, Roles) -> {ok, cta_auth_method()} when Config :: map(),
                                                                Roles :: map().
-callback welcome_challenge_or_abort(Session, Config, Realm) ->
                                        {ok, Session} |
                                        {challenge, AuthMethod, Extra, Session} |
                                        {abort, Reason} when Session :: cta_session(),
                                                             Config :: cta_auth_method(),
                                                             Realm :: cta_realm(),
                                                             AuthMethod :: binary(),
                                                             Extra :: map(),
                                                             Reason :: any().
-callback welcome_or_abort(Session, Authentication, Config, Realm) ->
                              {ok, Session} | {abort, Reason} when Session :: cta_session(),
                                                                   Authentication :: binary(),
                                                                   Config :: cta_auth_method(),
                                                                   Realm :: cta_realm(),
                                                                   Reason :: any().

create_methods(AuthConfigs, Roles) ->
    AuthTypes = maps:keys(AuthConfigs),
    CreateMethod =
        fun (Type, AuthConfMap) ->
                Conf = maps:get(Type, AuthConfigs),
                AuthConf = create_method(Type, Conf, Roles),
                maps:put(Type, AuthConf, AuthConfMap)
        end,
    lists:foldl(CreateMethod, #{}, AuthTypes).

create_method(anonymous, Config, Roles) ->
    cta_auth_anonymous:create(Config, Roles);
create_method(wampcra, Config, Roles) ->
    cta_auth_cra:create(Config, Roles);
create_method(Unknown, Config, _Roles) ->
    throw({unknown_auth_type, Unknown, Config}).

-spec welcome_challenge_or_abort(Session, AuthMethod, Realm) ->
                                    {ok, Session} |
                                    {challenge, AuthMethod, Extra, Session} |
                                    {abort, Reason} when Session :: cta_session(),
                                                         Realm :: cta_realm(),
                                                         AuthMethod :: binary(),
                                                         Extra :: map(),
                                                         Reason :: any().
welcome_challenge_or_abort(Session, AuthMethod, Realm) ->
    AuthConf = cta_realm:get_auth_method(AuthMethod, Realm),
    #cta_auth_method{module = Mod} = AuthConf,
    Mod:welcome_challenge_or_abort(Session, AuthConf, Realm).
