-module(cta_auth_cra).

-behaviour(cta_auth_method).

-define(NONCE_LENGTH, 32).

-export([create/2, welcome_challenge_or_abort/3, welcome_or_abort/4]).

-include("ct_auth.hrl").

-spec create(Config, Roles) -> cta_auth_method() when Config :: map(), Roles :: list().
create(ConfigIn, Roles) ->
    Auth = create_auth(ConfigIn, Roles),
    #cta_auth_method{type = wampcra, module = ?MODULE, auth = Auth}.

-spec welcome_challenge_or_abort(Session, Config, Realm) -> Result when Session ::
                                                                            cta_session(),
                                                                        Realm :: cta_realm(),
                                                                        Config :: map(),
                                                                        Result :: {ok, Session} |
                                                                                  {error, saving} |
                                                                                  {abort, canceled}.
welcome_challenge_or_abort(Session, #cta_auth_method{auth = {static, User}}, _Realm) ->
    Details = cta_session:get_details(Session),
    AuthId = maps:get(authid, Details),
    UserDetails = maps:get(AuthId, User, undefined),
    challenge_if_user_exists(UserDetails, AuthId, Session).

welcome_or_abort(_Session, {authenticate, _Sig, _Extra}, _Config, _Realm) ->
    {abort, cancelled}.

challenge_if_user_exists(undefined, _, _) ->
    {abort, cancelled};
challenge_if_user_exists(#{role := Role}, AuthId, Session0) ->
    {ok, Session} = cta_session:set_auth_details(AuthId, wampcra, ?MODULE, Session0),
    SessionId = cta_session:get_id(Session),
    Nonce = gen_nonce(),
    TimeStamp = gen_timestamp(),
    Challenge =
        #{authid => AuthId,
          authprovider => ?MODULE,
          nonce => Nonce,
          timestamp => TimeStamp,
          authrole => Role,
          authmethod => wampcra,
          session => SessionId},
    {challenge, wampcra, Challenge, Session}.

create_auth(#{user := User}, Roles) when is_list(User) ->
    {static, add_user_to_auth(User, Roles, #{})};
create_auth(#{authenticator := Authenticator}, _Roles) ->
    {dynamic, Authenticator}.

add_user_to_auth([], _Roles, Auth) ->
    Auth;
add_user_to_auth([User | T], Roles, Auth) ->
    NewAuth = add_valid_user_if_role_exists(User, Roles, Auth),
    add_user_to_auth(T, Roles, NewAuth).

add_valid_user_if_role_exists(#{id := _Id,
                                role := Role,
                                hash := _Hash,
                                salt := _Salt,
                                keylen := _KeyLen,
                                iterations := _Iterations} =
                                  User,
                              Roles,
                              Auth) ->
    RoleExists = lists:member(Role, Roles),
    add_if_role_exists(RoleExists, User, Auth);
add_valid_user_if_role_exists(_User, _Roles, Auth) ->
    Auth.

add_if_role_exists(true, #{id := Id} = UserAll, Auth) ->
    Keys = [role, hash, salt, keylen, interations],
    User = maps:with(Keys, UserAll),
    maps:put(Id, User, Auth);
add_if_role_exists(false, _User, Auth) ->
    Auth.

gen_timestamp() ->
    Now = erlang:system_time(second),
    calendar:system_time_to_rfc3339(Now).

gen_nonce() ->
    base64:encode(crypto:strong_rand_bytes(?NONCE_LENGTH)).
