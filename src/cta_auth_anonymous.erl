-module(cta_auth_anonymous).

-behaviour(cta_auth_method).

-export([create/2, welcome_challenge_or_abort/3, welcome_or_abort/4]).

-include("ct_auth.hrl").

-spec create(Config, Roles) -> cta_auth_method() when Config :: map(), Roles :: list().
create(ConfigIn, Roles) ->
    Auth = create_auth(ConfigIn, Roles),
    #cta_auth_method{type = anonymous, module = ?MODULE, auth = Auth}.

-spec welcome_challenge_or_abort(Session, Config, Realm) -> Result when Session ::
                                                                            cta_session(),
                                                                        Realm :: cta_realm(),
                                                                        Config :: map(),
                                                                        Result :: {ok, Session} |
                                                                                  {error, saving} |
                                                                                  {abort, canceled}.
welcome_challenge_or_abort(Session, #cta_auth_method{auth = {static, RoleName}}, Realm) ->
    {ok, NewSession} = cta_session:set_auth_details(anonymous, anonymous, ?MODULE, Session),
    {ok, Role} = cta_realm:get_role(RoleName, Realm),
    cta_session:authenticate(Role, NewSession).

welcome_or_abort(_Session, _Authentication, _Config, _Realm) ->
    {abort, cancelled}.

create_auth(#{role := Role}, Roles) ->
    create_auth_if_role_exists(lists:member(Role, Roles), Role);
create_auth(#{authenticator := Authenticator}, _Roles) ->
    {dynamic, Authenticator}.

create_auth_if_role_exists(true, Role) ->
    {static, Role};
create_auth_if_role_exists(_, Role) ->
    throw({role_not_found, Role}).
