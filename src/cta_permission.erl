-module(cta_permission).

-include("ct_auth.hrl").

-export([create/1, get_perm_for/3]).

-define(FORBID,
        #{allow_call => false,
          allow_register => false,
          allow_subscribe => false,
          allow_publish => false}).
-define(KEYS, [allow_call, allow_register, allow_subscribe, allow_publish]).

create(ConfigIn) ->
    Config = ensure_binary_keys(ConfigIn),
    Uris = maps:keys(Config),
    Empty = gb_trees:empty(),
    PermTree = create_permission_tree(Uris, Config, Empty),
    #cta_permission{tree = PermTree}.

get_perm_for(Operation, Uri, #cta_permission{tree = Tree}) ->
    UriList = create_uri_list(Uri),
    find_permission_for(UriList, Operation, Tree).

find_permission_for([], _Operation, _Tree) ->
    forbidden;
find_permission_for([Uri | T], Operation, Tree) ->
    MaybeValue = gb_trees:lookup(Uri, Tree),
    check_permission(MaybeValue, T, Operation, Tree).

check_permission(none, T, Operation, Tree) ->
    find_permission_for(T, Operation, Tree);
check_permission({value, Perm}, _T, Operation, _Tree) ->
    return_permission_for(Operation, Perm).

return_permission_for(call, #{allow_call := true}) ->
    allowed;
return_permission_for(register, #{allow_register := true}) ->
    allowed;
return_permission_for(subscribe, #{allow_subscribe := true}) ->
    allowed;
return_permission_for(publish, #{allow_publish := true}) ->
    allowed;
return_permission_for(_, _) ->
    forbidden.

create_uri_list(Uri) ->
    Parts = binary:split(Uri, <<".">>, [global]),
    combine_parts(Parts, <<"">>, [], Uri).

combine_parts([_Last], Current, UriList, Uri) ->
    Entry = create_sub_entry(Current),
    [Uri, Entry | UriList];
combine_parts([Part | T], Current, UriList, Uri) ->
    NewCurrent = create_sub_uri(Part, Current),
    Entry = create_sub_entry(Current),
    NewUriList = [Entry | UriList],
    combine_parts(T, NewCurrent, NewUriList, Uri).

create_sub_entry(<<"">>) ->
    <<"*">>;
create_sub_entry(Current) ->
    DotAsterisk = <<".*">>,
    <<Current/binary, DotAsterisk/binary>>.

create_sub_uri(Part, <<"">>) ->
    Part;
create_sub_uri(Part, Current) ->
    Dot = <<".">>,
    <<Current/binary, Dot/binary, Part/binary>>.

create_permission_tree([], _Config, PermTree) ->
    gb_trees:balance(PermTree);
create_permission_tree([Uri | T], Config, PermTree) ->
    Perm = maps:get(Uri, Config, #{}),
    UpdatedPerm = maps:with(?KEYS, Perm),
    Insert = contains_data(UpdatedPerm),
    NewPermTree = maybe_insert(Insert, Uri, UpdatedPerm, PermTree),
    create_permission_tree(T, Config, NewPermTree).

contains_data(Perm) ->
    A = maps:get(allow_call, Perm, false),
    B = maps:get(allow_register, Perm, false),
    C = maps:get(allow_subscribe, Perm, false),
    D = maps:get(allow_publish, Perm, false),
    A or B or C or D.

maybe_insert(true, Uri, Perm, Tree) ->
    gb_trees:insert(Uri, Perm, Tree);
maybe_insert(_, _Uri, _Perm, Tree) ->
    Tree.

ensure_binary_keys(Map) ->
    Keys = maps:keys(Map),
    ensure_binary_keys(Keys, Map).

ensure_binary_keys([], Map) ->
    Map;
ensure_binary_keys([Bin | T], Map) when is_binary(Bin) ->
    ensure_binary_keys(T, Map);
ensure_binary_keys([Atom | T], Map) when is_atom(Atom) ->
    Bin = atom_to_binary(Atom, utf8),
    Value = maps:get(Atom, Map),
    Map1 = maps:remove(Atom, Map),
    ensure_binary_keys(T, maps:put(Bin, Value, Map1)).
