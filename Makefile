REBAR = rebar3

.PHONY: all ct test clean elvis compile

all: compile elvis

clean:
	$(REBAR) cover -r 
	$(REBAR) clean

eunit:
	$(REBAR) eunit
	$(REBAR) cover -v

ct:
	$(REBAR) ct
	$(REBAR) cover -v

tests: elvis eunit dialyzer

elvis:
	$(REBAR) lint

dialyzer:
	$(REBAR) dialyzer 

compile:
	$(REBAR) compile
	$(REBAR) lint 
