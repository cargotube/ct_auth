
-record(cta_auth_method, { type = undefined,
			   module = undefined,
                           auth = undefined :: undefined | {static, binary()} | {dynamic, binary()}
                          }).

-record(cta_permission, {
          tree = undefined
	 }).

-record(cta_session, {
          id = undefined,
          realm = undefined,
          details = #{},
          disclose_caller = false,
          disclose_publisher = false,
          authid = undefined,
          authrole = undefined,
          authprovider = undefined,
          authmethod = undefined,
          authenticated = false,
	  permission = undefined,
          subscriptions = [],
          registrations = [],
	  invocations=[],
          transport = undefined,
	  peer_ip = undefined,
	  peer_port = undefined,
	  peer_cert = undefined
         }).


-type cta_session() :: #cta_session{}.
-type cta_realm() :: #cta_realm{}.
-type cta_role() :: #cta_role{}.
-type cta_auth_method() :: #cta_auth_method{}.
